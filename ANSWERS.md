### Nabenik test

2. Please describe briefly the main purpose for the following Jakarta EE specs, also add in your answer http links to actual implementations. Is this project using all specs?
- EJB
> Cubre aspectos como persistencia, seguridad e integridad de las transacciones. El proyecto actual no lo traia mas si lo necesita para guardar de forma temporal los registros.
- Servlet
> Modulo de Java que nos permite crear aplicaciones web dinamicas.
- CDI
- JAX-RS
> Set de interfaces y anotaciones que nos brinda java ee para definir y decidir el comportamiento de nuestros metodos.

3. Which of the following is an application server?

* Open Liberty
* Apache TomEE
* Eclipse Jetty
* Eclipse Glassfish
* Oracle Weblogic

> En mi experiencia se que Apache TomEE y Eclipse glassfish son servidores de aplicaciones.

4. In your opinion, what's the main benefit of moving from Java 11 to Java 17?
- Mantener el codigo de un proyecto actualizado, extendiendo asi su ciclo de vida
- Garantizar mayor seguridad a la aplicacion dada la etapa de soporte en que se encuentra y siendo esta la version LTS mas reciente.

5. Is it possible to run this project (as is) over Java 17? Why?
> No me permitio completar el comando `mvn clean package` dada un problema con la dependencia de surefire. Quiza lo sea removiendo la dependencia con el codigo que haga uso de ella

6. Is it possible to run this project with GraalVM Native? Why?

7. How do you run this project directly from CLI without configuring any application server? Why is it possible?
> Es posible utilizando payara micro, como se observa en la parte de este [video](https://youtu.be/W5HBCqJbyKs?t=417 "Youtube video") utilizando el siguiente commando 
`java -jar ~/javabin/payara.jar --deploy target/back-test.war --autobindhttp`

## Development tasks

To solve this questions please use Gitflow workflow, still, your answers should be in the current branch.

Please also include screenshots on every task. You don't need to execute every task to submit your pull request but feel free to do it :).

0. (easy) Show your terminal demonstrating your installation of OpenJDK 11
![alt text][java_installation]

1. (easy) Run this project using an IDE/Editor of your choice
![alt text][run]

2. (medium) Execute the movie endpoint operations using curl, if needed please also check/fix the code to be REST compliant
![alt text][movie_all]
![alt text][movie_create]
![alt text][movie_get]
![alt text][movie_update]
![alt text][movie_updated]
![alt text][movie_delete]
![alt text][movie_deleted]

3. (medium) Write an SPA application using Angular, the application should be a basic CRUD that uses Movie operations, upload this application to a new Bitbucket repo and include the link as answer

> Tuve complicaciones mas actualizare este apartado lo mas pronto posible, incluso si ya no es tomado en cuenta, disculpas

4. (medium) This project has been created using Java EE APIs, please move it to Jakarta EE APIs and switch it to a compatible implementation (if needed)

5. (hard) Please identify the Integration Test for `MovieRepository`, after that implement each of the non-included CRUD methods

6. (hard) Please write the Repository and Controller for Actor model, after that implement each of the CRUD methods using an Integration Test

> Metodos de actor (Sin integracion de tests)
![alt text][actor_all]
![alt text][actor_create]
![alt text][actor_update]
![alt text][actor_updated]
![alt text][actor_delete]

7. (hard) This project uses vanilla Java EE for Database Persistence, please integrate Testcontainers with MySQL for testing purposes

> No se si sea valido colocar dudas, agregue la dependencia de mysql y cambie el fichero persistence.xml mas al usar el comando `mvn clean package`
> el fichero persistence.xml volvia a sus valores originales ignorando los cambios

8. (nightmare) This source code includes only Java EE APIs, hence it's possible to port it to [Open Liberty](https://openliberty.io/). Do it and don't port Integration Tests 

[java_installation]: ./assets/img/java_installation.png "Java installation"
[run]: ./assets/img/project_payara_micro_5_running.png "Project running"

[movie_all]: ./assets/img/movie_listAll.png "movie all"
[movie_create]: ./assets/img/movie_create.png "movie create"
[movie_get]: ./assets/img/movie_get.png "movie get"
[movie_update]: ./assets/img/movie_update.png "movie update"
[movie_updated]: ./assets/img/movie_1_updated.png "movie updated"
[movie_delete]: ./assets/img/movie_delete.png "movie delete"
[movie_deleted]: ./assets/img/movie_1_deleted.png "movie deleted"

[actor_all]: ./assets/img/actor_listAll.png "actor all"
[actor_create]: ./assets/img/actor_create.png "actor create"
[actor_update]: ./assets/img/actor_update.png "actor update"
[actor_updated]: ./assets/img/actor_7_updated.png "actor updated"
[actor_delete]: ./assets/img/actor_delete.png "actor delete"


