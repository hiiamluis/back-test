package com.nabenik.repository;


import com.nabenik.model.Actor;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;

/*
@RequestScoped
@Default
*/
@Local
@Stateless
public class ActorRepository {

    @PersistenceContext(unitName = "back-PU")
    @Inject
    EntityManager em;

    public Actor findById(Long id){
        return em.find(Actor.class, id);
    }

    public void create(Actor actor){
        em.persist(actor);
    }

    public Actor update(Long id, Actor actor){
        Actor stored = findById(id);
        stored.setName(actor.getName());
        stored.setCountry(actor.getCountry());
        stored.setBirthday(actor.getBirthday());
        return em.merge(actor);
        // return stored;
    }

    public void delete(Long id){
        Actor actor = findById(id);
        em.remove(actor);
    }

    public List<Actor> listAll(){
        String selection = "SELECT a FROM Actor a";
        Query query = em.createQuery(selection);
        return query.getResultList();
    }


    public List<Actor> listAll(String name){
        String query = "SELECT a FROM Actor a WHERE a.name LIKE :name";
        Query typedQuery = em.createQuery(query).setParameter("name", "%" + name + "%");
        return typedQuery.getResultList();
    }
}
