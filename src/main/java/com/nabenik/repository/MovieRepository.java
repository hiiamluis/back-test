package com.nabenik.repository;


import com.nabenik.model.Movie;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;

/*
@RequestScoped
@Default
*/
@Local
@Stateless
public class MovieRepository {

    @PersistenceContext(unitName = "back-PU")
    @Inject
    EntityManager em;

    public Movie findById(Long id){
        return em.find(Movie.class, id);
    }

    public void create(Movie movie){
        em.persist(movie);
    }

    public Movie update(Long id, Movie movie){
        Movie stored = findById(id);
        stored.setTitle(movie.getTitle());
        stored.setYear(movie.getYear());
        stored.setDuration(movie.getDuration());
        return em.merge(movie);
        // return stored;
    }

    public void delete(Long id){
        Movie movie = findById(id);
        em.remove(movie);
    }

    public List<Movie> listAll(){
        String selection = "SELECT m FROM Movie m";
        Query query = em.createQuery(selection);
        return query.getResultList();
    }


    public List<Movie> listAll(String title){
        String query = "SELECT m FROM Movie m WHERE m.title LIKE :title";
        Query typedQuery = em.createQuery(query).setParameter("title", "%" + title + "%");
        return typedQuery.getResultList();
    }
}
